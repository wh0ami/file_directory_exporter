#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Python3 File and Directory Exporter for Prometheus

#     Dev: wh0ami
# Licence: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/file_directory_exporter

import os


class DirectoryExporter:
    def __init__(self, input_path_list: list) -> None:
        # store the input_path_list in an attribute and create an empty output attribute
        self.__input_path_list = input_path_list
        self.__output = ""

    # method for getting the current output variable
    def get_output(self):
        return self.__output

    # method for executing the measurement
    def execute_measurement(self):
        # initialize attribute for the output string
        self.__output = "# DirectoryExporter for prometheus\n\n"

        # iterate over input_path_list and execute checks
        for directory in self.__input_path_list:
            # append the directory caption line to the output
            self.__output += f"# directory checks for {directory}\n"

            # check whether the directory exists
            self.__output += 'directory_exist{"' + directory + '"} '
            self.__output += '1' if os.path.isdir(directory) else '0'
            self.__output += '\n'

            # count the elements that the directory contains
            self.__output += self.__count_directory_elements(directory)

            # append line break after all checks for a directory to the output string
            self.__output += "\n"

        # append an additional line break to get an optical difference between the two exporters
        self.__output += "\n"

    # method for counting the elements in a directory
    @staticmethod
    def __count_directory_elements(directory_path: str) -> str:
        # internal counters
        counters = {"directories": {"hidden": 0, "normal": 0}, "files": {"hidden": 0, "normal": 0},
            "links": {"hidden": 0, "normal": 0}, "others": {"hidden": 0, "normal": 0}}
        output = ""

        # check, whether the directory is existing and accessible
        if os.path.isdir(directory_path):
            # scan the directory content into a list
            content = os.scandir(directory_path)

            # iterate over all elements in the directory
            for element in content:
                # check whether the element is hidden
                filestate = "hidden" if element.name.startswith(".") else "normal"

                # check the filestate of the element and increase the counter
                if element.is_dir():
                    counters["directories"][filestate] += 1
                elif element.is_file():
                    counters["files"][filestate] += 1
                elif element.is_symlink():
                    counters["links"][filestate] += 1
                else:
                    counters["others"][filestate] += 1

            # create the output strings based on the content of the counters dict
            for filetype in counters.keys():
                # add an all counter to each file type (it sums up the items independent of their state)
                counters[filetype]["all"] = sum(counters[filetype].values())

                # iterate over all file states and append an output string
                for filestate in counters[filetype].keys():
                    output += 'directory_contains_elements_number{directory="' + directory_path + '",type="' + filetype + '",state="' + filestate + '"} ' + str(
                        counters[filetype][filestate]) + "\n"

        # return the output string - it is also possible, that this string stays empty
        return output
