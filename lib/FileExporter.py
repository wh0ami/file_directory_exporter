#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Python3 File and Directory Exporter for Prometheus

#     Dev: wh0ami
# Licence: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/file_directory_exporter

import os
import re


class FileExporter:
    def __init__(self, input_pattern_list: list, file_exporter_config: dict) -> None:
        # store the input_pattern_list and the file exporter config in an attribute and create an empty output attribute
        self.__input_pattern_list = input_pattern_list
        self.__file_exporter_config = file_exporter_config
        self.__output = ""

    # method for getting the current output variable
    def get_output(self):
        return self.__output

    # method for executing the measurement
    def execute_measurement(self):
        # initialize attribute for the output string
        self.__output = "# FileExporter for prometheus\n\n"

        # iterate over input_pattern_list and execute checks
        for entry in self.__input_pattern_list:
            # initialize vars
            rootdir = entry[0]
            file_pattern = entry[1]

            # append the directory caption line to the output
            self.__output += f"# file checks for pattern '{file_pattern}' in {rootdir}\n"

            # obtain a file list for the given pattern in the given rootdir
            files = self.__find_files_based_on_pattern(rootdir, file_pattern)
            self.__output += 'file_matches{rootdir="' + rootdir + '",pattern="' + file_pattern + '"} ' + str(
                len(files)) + '\n'

            # iterate over all files from the result list, if we need to check age or size
            if True in self.__file_exporter_config.values():
                for element in files:
                    if self.__file_exporter_config["age"]:
                        self.__output += 'file_age_seconds{rootdir="' + rootdir + '",file="' + element.name + '"} ' + \
                                         str(int(round(element.stat().st_mtime, 0))) + '\n'
                    if self.__file_exporter_config["size"]:
                        self.__output += 'file_size_bytes{rootdir="' + rootdir + '",file="' + element.name + '"} ' + \
                                         str(element.stat().st_size) + '\n'
                    if self.__file_exporter_config["uid"]:
                        self.__output += 'file_owner_uid{rootdir="' + rootdir + '",file="' + element.name + '"} ' + \
                                         str(element.stat().st_uid) + '\n'
                    if self.__file_exporter_config["gid"]:
                        self.__output += 'file_group_gid{rootdir="' + rootdir + '",file="' + element.name + '"} ' + \
                                         str(element.stat().st_gid) + '\n'

            # append line break after all checks for a file pattern to the output string
            self.__output += "\n"

        # append an additional line break to get an optical difference between the two exporters
        self.__output += "\n"

    # method for counting files matching a regex pattern
    @staticmethod
    def __find_files_based_on_pattern(rootdir: str, file_pattern: str) -> list:
        # prepare the regex pattern and the empty result list to use it
        regex = re.compile(f'^{file_pattern}$')
        result = []

        # fetch a content list from the rootdir and iterate over it - if a file matches the pattern, append it to the
        # result list
        try:
            for element in os.scandir(rootdir):
                if element.is_file() and bool(regex.match(element.name)):
                    result.append(element)
        except PermissionError:
            pass
        except FileNotFoundError:
            pass

        # return the result list
        return result
