#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Python3 File and Directory Exporter for Prometheus

#     Dev: wh0ami
# Licence: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/file_directory_exporter

import socketserver
from http.server import BaseHTTPRequestHandler

from lib import DirectoryExporter, FileExporter


# create a custom class for the request handler of the webserver
# standard function (GET request) are overloaded by our own procedures
class CustomRequestHandler(BaseHTTPRequestHandler):
    # initialize custom config as an empty dict
    custom_config = None
    __directory_exporter = None
    __file_exporter = None

    # overload the constructor with additional, custom statements (initialize the exporters)
    def __init__(self, request: bytes, client_address: tuple[str, int], server: socketserver.BaseServer):
        self.__directory_exporter = DirectoryExporter.DirectoryExporter(
            self.custom_config["directory_exporter_input"])
        self.__file_exporter = FileExporter.FileExporter(
            self.custom_config["file_exporter_regex_input"], self.custom_config["file_exporter_config"])
        super().__init__(request, client_address, server)

    # overload the do_GET method with our custom procedure
    def do_GET(self):
        if self.path == "/metrics":
            # send HTTP status code and headers
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            # execute FileExporter and send its response
            self.__file_exporter.execute_measurement()
            self.wfile.write(bytes(self.__file_exporter.get_output(), "utf-8"))
            # execute DirectoryExporter and send its response
            self.__directory_exporter.execute_measurement()
            self.wfile.write(bytes(self.__directory_exporter.get_output(), "utf-8"))
        else:
            # send HTTP status code 404, if the requested path doesn't match /metrics
            self.send_response(404)
