# File and Directory Exporter for Prometheus

This is an exporter for monitoring files and directories with prometheus.

Features:
- DirectoryExporter
  - check, whether a directory exists
  - count the items in a directory
- FileExporter
  - count files, that are matching a RegEx pattern in a specified directory
  - check the age and/or size of files
  - check the owner and/or group of a file

### Setup

```shell
root@testpi:/# cd <your app directory>
root@testpi:/opt# git clone https://codeberg.org/wh0ami/file_directory_exporter.git
root@testpi:/opt# cd file_directory_exporter
root@testpi:/opt/file_directory_exporter# useradd -d $(pwd) --shell /bin/false file_directory_exporter
root@testpi:/opt/file_directory_exporter# usermod -L file_directory_exporter
root@testpi:/opt/file_directory_exporter# chown -R file_directory_exporter:file_directory_exporter .
root@testpi:/opt/file_directory_exporter# cp config.sample.json config.json
root@testpi:/opt/file_directory_exporter# chmod +x exporter.py
root@testpi:/opt/file_directory_exporter# cp file_directory_exporter.service.sample /etc/systemd/system/file_directory_exporter.service
root@testpi:/opt/file_directory_exporter# vi /etc/systemd/system/file_directory_exporter.service # fit this file to your setup
root@testpi:/opt/file_directory_exporter# systemctl daemon-reload
root@testpi:/opt/file_directory_exporter# systemctl enable --now file_directory_exporter
```

The service should now run on the socket, that is configured in the `config.json`.
You can customize the listen ip and the used port in the `exporter.py` file.

The files, directories and checks can be configured in `config.json`.

### Example output

```
# FileExporter for prometheus

# file checks for pattern 'fstab' in /etc/
file_matches{rootdir="/etc/",pattern="fstab"} 1
file_age_seconds{rootdir="/etc/",file="fstab"} 1622886141
file_size_bytes{rootdir="/etc/",file="fstab"} 549
file_owner_uid{rootdir="/etc/",file="fstab"} 0
file_group_gid{rootdir="/etc/",file="fstab"} 0

# file checks for pattern '.*\.conf' in /etc/
file_matches{rootdir="/etc/",pattern=".*\.conf"} 39
file_age_seconds{rootdir="/etc/",file="sysctl.conf"} 1589683407
file_size_bytes{rootdir="/etc/",file="sysctl.conf"} 2355
file_owner_uid{rootdir="/etc/",file="sysctl.conf"} 0
file_group_gid{rootdir="/etc/",file="sysctl.conf"} 0
file_age_seconds{rootdir="/etc/",file="deluser.conf"} 1537049659
file_size_bytes{rootdir="/etc/",file="deluser.conf"} 604
file_owner_uid{rootdir="/etc/",file="deluser.conf"} 0
file_group_gid{rootdir="/etc/",file="deluser.conf"} 0
file_age_seconds{rootdir="/etc/",file="pam.conf"} 1535067473
file_size_bytes{rootdir="/etc/",file="pam.conf"} 552
file_owner_uid{rootdir="/etc/",file="pam.conf"} 0
file_group_gid{rootdir="/etc/",file="pam.conf"} 0
file_age_seconds{rootdir="/etc/",file="hdparm.conf"} 1608069716
file_size_bytes{rootdir="/etc/",file="hdparm.conf"} 4436
file_owner_uid{rootdir="/etc/",file="hdparm.conf"} 0
file_group_gid{rootdir="/etc/",file="hdparm.conf"} 0
file_age_seconds{rootdir="/etc/",file="gai.conf"} 1612199733
file_size_bytes{rootdir="/etc/",file="gai.conf"} 2584
file_owner_uid{rootdir="/etc/",file="gai.conf"} 0
file_group_gid{rootdir="/etc/",file="gai.conf"} 0
file_age_seconds{rootdir="/etc/",file="e2scrub.conf"} 1613986251
file_size_bytes{rootdir="/etc/",file="e2scrub.conf"} 685
file_owner_uid{rootdir="/etc/",file="e2scrub.conf"} 0
file_group_gid{rootdir="/etc/",file="e2scrub.conf"} 0
file_age_seconds{rootdir="/etc/",file="smartd.conf"} 1648201871
file_size_bytes{rootdir="/etc/",file="smartd.conf"} 7042
file_owner_uid{rootdir="/etc/",file="smartd.conf"} 0
file_group_gid{rootdir="/etc/",file="smartd.conf"} 0
file_age_seconds{rootdir="/etc/",file="sensors3.conf"} 1611986725
file_size_bytes{rootdir="/etc/",file="sensors3.conf"} 10593
file_owner_uid{rootdir="/etc/",file="sensors3.conf"} 0
file_group_gid{rootdir="/etc/",file="sensors3.conf"} 0
file_age_seconds{rootdir="/etc/",file="host.conf"} 1565812401
file_size_bytes{rootdir="/etc/",file="host.conf"} 92
file_owner_uid{rootdir="/etc/",file="host.conf"} 0
file_group_gid{rootdir="/etc/",file="host.conf"} 0
file_age_seconds{rootdir="/etc/",file="logrotate.conf"} 1621328961
file_size_bytes{rootdir="/etc/",file="logrotate.conf"} 592
file_owner_uid{rootdir="/etc/",file="logrotate.conf"} 0
file_group_gid{rootdir="/etc/",file="logrotate.conf"} 0
file_age_seconds{rootdir="/etc/",file="chromium-flags.conf"} 1635857044
file_size_bytes{rootdir="/etc/",file="chromium-flags.conf"} 400
file_owner_uid{rootdir="/etc/",file="chromium-flags.conf"} 0
file_group_gid{rootdir="/etc/",file="chromium-flags.conf"} 0
file_age_seconds{rootdir="/etc/",file="apg.conf"} 1506896380
file_size_bytes{rootdir="/etc/",file="apg.conf"} 433
file_owner_uid{rootdir="/etc/",file="apg.conf"} 0
file_group_gid{rootdir="/etc/",file="apg.conf"} 0
file_age_seconds{rootdir="/etc/",file="sudo.conf"} 1644839285
file_size_bytes{rootdir="/etc/",file="sudo.conf"} 4573
file_owner_uid{rootdir="/etc/",file="sudo.conf"} 0
file_group_gid{rootdir="/etc/",file="sudo.conf"} 0
file_age_seconds{rootdir="/etc/",file="debconf.conf"} 1587208208
file_size_bytes{rootdir="/etc/",file="debconf.conf"} 2969
file_owner_uid{rootdir="/etc/",file="debconf.conf"} 0
file_group_gid{rootdir="/etc/",file="debconf.conf"} 0
file_age_seconds{rootdir="/etc/",file="rygel.conf"} 1613820271
file_size_bytes{rootdir="/etc/",file="rygel.conf"} 5620
file_owner_uid{rootdir="/etc/",file="rygel.conf"} 0
file_group_gid{rootdir="/etc/",file="rygel.conf"} 0
file_age_seconds{rootdir="/etc/",file="libaudit.conf"} 1611456087
file_size_bytes{rootdir="/etc/",file="libaudit.conf"} 191
file_owner_uid{rootdir="/etc/",file="libaudit.conf"} 0
file_group_gid{rootdir="/etc/",file="libaudit.conf"} 0
file_age_seconds{rootdir="/etc/",file="ucf.conf"} 1592285873
file_size_bytes{rootdir="/etc/",file="ucf.conf"} 1260
file_owner_uid{rootdir="/etc/",file="ucf.conf"} 0
file_group_gid{rootdir="/etc/",file="ucf.conf"} 0
file_age_seconds{rootdir="/etc/",file="adduser.conf"} 1618915172
file_size_bytes{rootdir="/etc/",file="adduser.conf"} 3028
file_owner_uid{rootdir="/etc/",file="adduser.conf"} 0
file_group_gid{rootdir="/etc/",file="adduser.conf"} 0
file_age_seconds{rootdir="/etc/",file="fprintd.conf"} 1610540604
file_size_bytes{rootdir="/etc/",file="fprintd.conf"} 20
file_owner_uid{rootdir="/etc/",file="fprintd.conf"} 0
file_group_gid{rootdir="/etc/",file="fprintd.conf"} 0
file_age_seconds{rootdir="/etc/",file="appstream.conf"} 1613503306
file_size_bytes{rootdir="/etc/",file="appstream.conf"} 769
file_owner_uid{rootdir="/etc/",file="appstream.conf"} 0
file_group_gid{rootdir="/etc/",file="appstream.conf"} 0
file_age_seconds{rootdir="/etc/",file="sudo_logsrvd.conf"} 1644839285
file_size_bytes{rootdir="/etc/",file="sudo_logsrvd.conf"} 9390
file_owner_uid{rootdir="/etc/",file="sudo_logsrvd.conf"} 0
file_group_gid{rootdir="/etc/",file="sudo_logsrvd.conf"} 0
file_age_seconds{rootdir="/etc/",file="xattr.conf"} 1648028509
file_size_bytes{rootdir="/etc/",file="xattr.conf"} 681
file_owner_uid{rootdir="/etc/",file="xattr.conf"} 0
file_group_gid{rootdir="/etc/",file="xattr.conf"} 0
file_age_seconds{rootdir="/etc/",file="ca-certificates.conf"} 1652369596
file_size_bytes{rootdir="/etc/",file="ca-certificates.conf"} 5914
file_owner_uid{rootdir="/etc/",file="ca-certificates.conf"} 0
file_group_gid{rootdir="/etc/",file="ca-certificates.conf"} 0
file_age_seconds{rootdir="/etc/",file="ld.so.conf"} 1617198268
file_size_bytes{rootdir="/etc/",file="ld.so.conf"} 34
file_owner_uid{rootdir="/etc/",file="ld.so.conf"} 0
file_group_gid{rootdir="/etc/",file="ld.so.conf"} 0
file_age_seconds{rootdir="/etc/",file="lftp.conf"} 1633601633
file_size_bytes{rootdir="/etc/",file="lftp.conf"} 3309
file_owner_uid{rootdir="/etc/",file="lftp.conf"} 0
file_group_gid{rootdir="/etc/",file="lftp.conf"} 0
file_age_seconds{rootdir="/etc/",file="kernel-img.conf"} 1622886240
file_size_bytes{rootdir="/etc/",file="kernel-img.conf"} 110
file_owner_uid{rootdir="/etc/",file="kernel-img.conf"} 0
file_group_gid{rootdir="/etc/",file="kernel-img.conf"} 0
file_age_seconds{rootdir="/etc/",file="mke2fs.conf"} 1641672156
file_size_bytes{rootdir="/etc/",file="mke2fs.conf"} 744
file_owner_uid{rootdir="/etc/",file="mke2fs.conf"} 0
file_group_gid{rootdir="/etc/",file="mke2fs.conf"} 0
file_age_seconds{rootdir="/etc/",file="usb_modeswitch.conf"} 1603901431
file_size_bytes{rootdir="/etc/",file="usb_modeswitch.conf"} 1523
file_owner_uid{rootdir="/etc/",file="usb_modeswitch.conf"} 0
file_group_gid{rootdir="/etc/",file="usb_modeswitch.conf"} 0
file_age_seconds{rootdir="/etc/",file="nftables.conf"} 1648034938
file_size_bytes{rootdir="/etc/",file="nftables.conf"} 228
file_owner_uid{rootdir="/etc/",file="nftables.conf"} 0
file_group_gid{rootdir="/etc/",file="nftables.conf"} 0
file_age_seconds{rootdir="/etc/",file="nsswitch.conf"} 1622886301
file_size_bytes{rootdir="/etc/",file="nsswitch.conf"} 542
file_owner_uid{rootdir="/etc/",file="nsswitch.conf"} 0
file_group_gid{rootdir="/etc/",file="nsswitch.conf"} 0
file_age_seconds{rootdir="/etc/",file="fuse.conf"} 1648043594
file_size_bytes{rootdir="/etc/",file="fuse.conf"} 694
file_owner_uid{rootdir="/etc/",file="fuse.conf"} 0
file_group_gid{rootdir="/etc/",file="fuse.conf"} 0
file_age_seconds{rootdir="/etc/",file="resolv.conf"} 1660153599
file_size_bytes{rootdir="/etc/",file="resolv.conf"} 1180
file_owner_uid{rootdir="/etc/",file="resolv.conf"} 101
file_group_gid{rootdir="/etc/",file="resolv.conf"} 103
file_age_seconds{rootdir="/etc/",file="kerneloops.conf"} 1617809284
file_size_bytes{rootdir="/etc/",file="kerneloops.conf"} 1308
file_owner_uid{rootdir="/etc/",file="kerneloops.conf"} 0
file_group_gid{rootdir="/etc/",file="kerneloops.conf"} 0
file_age_seconds{rootdir="/etc/",file="brltty.conf"} 1647622799
file_size_bytes{rootdir="/etc/",file="brltty.conf"} 29219
file_owner_uid{rootdir="/etc/",file="brltty.conf"} 0
file_group_gid{rootdir="/etc/",file="brltty.conf"} 0
file_age_seconds{rootdir="/etc/",file="proxychains.conf"} 1643922327
file_size_bytes{rootdir="/etc/",file="proxychains.conf"} 1678
file_owner_uid{rootdir="/etc/",file="proxychains.conf"} 0
file_group_gid{rootdir="/etc/",file="proxychains.conf"} 0
file_age_seconds{rootdir="/etc/",file="pnm2ppa.conf"} 1652369737
file_size_bytes{rootdir="/etc/",file="pnm2ppa.conf"} 7649
file_owner_uid{rootdir="/etc/",file="pnm2ppa.conf"} 0
file_group_gid{rootdir="/etc/",file="pnm2ppa.conf"} 0
file_age_seconds{rootdir="/etc/",file="rsyslog.conf"} 1614187821
file_size_bytes{rootdir="/etc/",file="rsyslog.conf"} 1382
file_owner_uid{rootdir="/etc/",file="rsyslog.conf"} 0
file_group_gid{rootdir="/etc/",file="rsyslog.conf"} 0
file_age_seconds{rootdir="/etc/",file="libao.conf"} 1516305034
file_size_bytes{rootdir="/etc/",file="libao.conf"} 27
file_owner_uid{rootdir="/etc/",file="libao.conf"} 0
file_group_gid{rootdir="/etc/",file="libao.conf"} 0
file_age_seconds{rootdir="/etc/",file="smi.conf"} 1543758348
file_size_bytes{rootdir="/etc/",file="smi.conf"} 1201
file_owner_uid{rootdir="/etc/",file="smi.conf"} 0
file_group_gid{rootdir="/etc/",file="smi.conf"} 0

# file checks for pattern '' in /etc/
file_matches{rootdir="/etc/",pattern=""} 0

# file checks for pattern 'baum' in /etc/
file_matches{rootdir="/etc/",pattern="baum"} 0

# file checks for pattern 'test' in /root/test
file_matches{rootdir="/root/test",pattern="test"} 0


# DirectoryExporter for prometheus

# directory checks for /etc/
directory_exist{"/etc/"} 1
directory_contains_elements_number{directory="/etc/",type="directories",state="hidden"} 1
directory_contains_elements_number{directory="/etc/",type="directories",state="normal"} 155
directory_contains_elements_number{directory="/etc/",type="directories",state="all"} 156
directory_contains_elements_number{directory="/etc/",type="files",state="hidden"} 1
directory_contains_elements_number{directory="/etc/",type="files",state="normal"} 110
directory_contains_elements_number{directory="/etc/",type="files",state="all"} 111
directory_contains_elements_number{directory="/etc/",type="links",state="hidden"} 0
directory_contains_elements_number{directory="/etc/",type="links",state="normal"} 0
directory_contains_elements_number{directory="/etc/",type="links",state="all"} 0
directory_contains_elements_number{directory="/etc/",type="others",state="hidden"} 0
directory_contains_elements_number{directory="/etc/",type="others",state="normal"} 0
directory_contains_elements_number{directory="/etc/",type="others",state="all"} 0

# directory checks for /home/pi/
directory_exist{"/home/pi/"} 0

# directory checks for /app
directory_exist{"/app"} 0
```

### FAQ

Q: What if the user that executes the exporter have no permissions for a file/directory?  
A: The file/directory will be displayed as non-existent.

Q: Which elements will the directory exporter count?  
A: Files, hidden files and directories (except `.` and `..`)

Q: Does the file age means the point of creation or the point of the last modification.  
A: It will display the timespan until the last point of modification (in seconds, mtime). You may read Python3 docs
for [`os.path.getmtime()`](https://docs.python.org/3/library/os.path.html#os.path.getmtime).

Q: How to update the exporter?  
A: Go to the directory of the exporter and run `git pull` and `systemctl restart file_directory_exporter.service`.

Q: What if the config file could not be found?  
A: The exporter will stop directly after starting with exit code 1.

Q: What if I don't want to scan directories, just files? (or vice versa)  
A: Just remove all entries in the respective section (but don't remove the complete section) in the config file.

Q: Does this exporter also run on windows?  
A: This is currently untested. But feel free to try and tell me about your experiences.

