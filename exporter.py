#! /usr/bin/python3
# -*- coding: utf-8 -*-

# Python3 File and Directory Exporter for Prometheus

#     Dev: wh0ami
# Licence: Public Domain <https://unlicense.org>
# Project: https://codeberg.org/wh0ami/file_directory_exporter

import json
import logging
import sys
from http.server import HTTPServer
from socket import AF_INET, AF_INET6

from lib import WebServer

if __name__ == "__main__":
    # initialize the logger
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)

    # try to load the config file
    try:
        # read the config path from the cli parameter
        configfile = sys.argv[1]

        # open the config file and read the json formatted content into a dict
        with open(configfile, "r") as infile:
            config = json.load(infile)
    except IndexError:
        logging.error("[Error] Please pass the path to your config file as the first parameter! (e.g. \"./exporter.py "
                      "config.json\")")
        sys.exit(1)
    except FileNotFoundError:
        logging.error("[Error] Config file not found! Usage: ./exporter.py <config>")
        sys.exit(1)
    except PermissionError:
        logging.error("[Error] Cannot access the specified config file (permission denied)!")
        sys.exit(1)
    except json.decoder.JSONDecodeError:
        logging.error("[Error] Config file contains malformed JSON and is thereby unreadable.")
        sys.exit(1)

    # identify, whether a IPv6 or a IPv4 address is configured as the host and configure the webserver according to
    # that fact + prepare the socket variable for logging
    if config['server_config']['host'].__contains__(':'):
        HTTPServer.address_family = AF_INET6
        socket = f"[{config['server_config']['host']}]"
    else:
        HTTPServer.address_family = AF_INET
        socket = config['server_config']['host']

    # put our custom_config into our custom request handler
    WebServer.CustomRequestHandler.custom_config = config

    # start the webserver
    webserver = HTTPServer((config["server_config"]["host"], config["server_config"]["port"]),
                           WebServer.CustomRequestHandler)
    logging.info(
        f"File and Directory Exporter for Prometheus started at http://{socket}:{config['server_config']['port']}")

    # run the webserver, until it is going to be stopped by ctrl-c/sigterm/whatever
    try:
        webserver.serve_forever()
    except KeyboardInterrupt:
        pass
    finally:
        webserver.server_close()
        logging.info("File and Directory Exporter for Prometheus stopped")
